package br.com.itau.consumer;

import br.com.itau.Acesso.Model.Acesso;
import br.com.itau.Logs.Log;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;

@Component
public class AcessoConsumer {

    @KafkaListener(topics = "spec4-anderson-fiuza-2", groupId = "acesso")
    public void receber(@Payload Acesso acesso) {

        Log log = new  Log();
        log.guardaLogs(acesso);

    }

}
