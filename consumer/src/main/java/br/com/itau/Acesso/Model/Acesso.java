package br.com.itau.Acesso.Model;

public class Acesso {


    private int id;
    private int  portaid;
    private int clienteid;
    
    private Boolean acessoautorizado ;

    public Boolean getAcessoautorizado() {
        return acessoautorizado;
    }

    public void setAcessoautorizado(Boolean acessoautorizado) {
        this.acessoautorizado = acessoautorizado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPortaid() {
        return portaid;
    }

    public void setPortaid(int portaid) {
        this.portaid = portaid;
    }

    public int getClienteid() {
        return clienteid;
    }

    public void setClienteid(int clienteid) {
        this.clienteid = clienteid;
    }
}
